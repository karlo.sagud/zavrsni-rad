package com.karlo;

import java.io.IOException;
import java.time.LocalTime;
import java.util.ArrayList;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;

@WebServlet("/feedback")

public class FeedbackServlet extends HttpServlet {
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session=request.getSession();
		String myurl = (String) request.getParameter("myurl");
		String poslanOdgovor = request.getParameter("odabranaDijagnoza");
		String tocnaDijagnoza = (String) session.getAttribute("tocnaDijagnoza");
		String workingDirectory = (String) session.getAttribute("workingDirectory");
		String workingCase = (String) session.getAttribute("workingCase");
		ArrayList <ArrayList<Parameter>> otherCases = (ArrayList<ArrayList<Parameter>>) session.getAttribute("otherCases");
		ArrayList otherCasesDiagnoses = (ArrayList) session.getAttribute("otherCasesDiagnoses");
		ArrayList<String> redoslijedPozvanihParametara = (ArrayList<String>) session.getAttribute("redoslijedPozvanihParametara");
		ArrayList<Double> vjerojatnostIspravno = (ArrayList<Double>) session.getAttribute("vjerojatnostIspravno");
		ArrayList<Integer> redoslijedCijenaPozvanihParametara = (ArrayList<Integer>) session.getAttribute("redoslijedCijenaPozvanihParametara");
		ArrayList<String> redoslijedVrijednostiPozvanihParametara = (ArrayList<String>) session.getAttribute("redoslijedVrijednostiPozvanihParametara");
		ArrayList<String> wantedDiagnosesList = (ArrayList<String>) session.getAttribute("wantedDiagnosesList");
		ArrayList<DiagnosisCaseNo> diagnosisSortWanted = (ArrayList<DiagnosisCaseNo>) session.getAttribute("diagnosisSortWanted");
		LocalTime timeStart = (LocalTime) session.getAttribute("timeStart");
		String urlPath = (String) session.getAttribute("urlPath");
		String rootPath = (String) session.getAttribute("rootPath");
		LinkedListParameters list=(LinkedListParameters) session.getAttribute("list");
		ArrayList listParameter = (ArrayList) session.getAttribute("listParameter");
		ArrayList<LocalTime> timeTrack = (ArrayList<LocalTime>) session.getAttribute("timeTrack");
		ArrayList<Double> vjerojatnostIspravnoKriviOdgovor = new ArrayList<Double>();
		ArrayList<LocalTime> timeOnParameter = new ArrayList<LocalTime>();
		ArrayList<String> redundantniParametri = new ArrayList<String>();
		ArrayList<String> bitniParametri =  new ArrayList<String>();
		ArrayList <Parameter> temp1 = new ArrayList <Parameter>();
		ArrayList <ArrayList<Parameter>> izbaceniOtherCases = (ArrayList<ArrayList<Parameter>>) session.getAttribute("izbaceniOtherCases");
		ArrayList<String> izbaceniOtherCasesDiagnoses = (ArrayList) session.getAttribute("izbaceniOtherCasesDiagnoses");
		ArrayList <Integer> possibleSolutions = new ArrayList<Integer>();
		ArrayList <Double> possibleSolutionsVjerojatnost = new ArrayList<Double>();
		ArrayList<DiagnosisCaseNo> diagnosisSortWantedCopy = new ArrayList<DiagnosisCaseNo>(diagnosisSortWanted);
		String lang = (String) session.getAttribute("lang");
		ArrayList <Parameter> nepozvaniParametri = new ArrayList <Parameter>();
		ArrayList listPrint =(ArrayList) session.getAttribute("listPrint");
		String odabranaDijagnoza = new String(poslanOdgovor);
		String errorP = new String(); // string koji opisuje grešku 
		int sameValuesWrongAnswer=0;
		int nevaljanaDijagnoza=0;

		if (listPrint.size()==0) {
			errorP="errorNoParameters";
			session.setAttribute("errorP", errorP);
			request.getRequestDispatcher("/rjesavanjeSlucaja.jsp").forward(request, response);
		}
		else {
			for (int i=0;i<wantedDiagnosesList.size();i++) {
				if (poslanOdgovor.equals(wantedDiagnosesList.get(i))) {
					nevaljanaDijagnoza++;
				}
			}
			if (nevaljanaDijagnoza == 0) {
				errorP="errorDiagnosis";
				session.setAttribute("odabranaDijagnoza", odabranaDijagnoza);
				session.setAttribute("errorP", errorP);
				request.getRequestDispatcher("/rjesavanjeSlucaja.jsp").forward(request, response);
			}
			else {
				int velicina=otherCases.size()+1;
				int size = diagnosisSortWanted.size();
				for(int i=0;i<size;i++) {
					if ((diagnosisSortWanted.get(i).dijagnoza).equals(tocnaDijagnoza)) {
						diagnosisSortWanted.remove(i);
						size=diagnosisSortWanted.size();
						i--;
					}
				}
				if (!(izbaceniOtherCases.isEmpty())) {
					for (int i=0;i<izbaceniOtherCases.size();i++) {
						otherCases.add(izbaceniOtherCases.get(i));
						otherCasesDiagnoses.add(izbaceniOtherCasesDiagnoses.get(i));
					}
				}
				size = diagnosisSortWanted.size();
				int brojac=0;
				for (int i=0;i<size;i++) {
					for (int j=i;j<size;j++) {
						if (diagnosisSortWanted.get(i).dijagnoza.equals(diagnosisSortWanted.get(j).dijagnoza)) {
							brojac++;
							if (j>i) {
								diagnosisSortWanted.remove(j);
								size = diagnosisSortWanted.size();
								j--;
							}
						}
					}
					possibleSolutions.add(brojac);
					brojac=0;
				}
				for (int i =0;i<diagnosisSortWanted.size();i++) {
					double temp = (double)(possibleSolutions.get(i))*100/(velicina);
					possibleSolutionsVjerojatnost.add(temp);
				}

				//possibleSolutions indeks odgovara diagnosisSortWanted
				if (myurl != null) {
					int firstIndex = myurl.indexOf("DSAT/") + 5; 		
					String substring = myurl.substring(firstIndex);		
					myurl = myurl.replace(substring, "");
				}
				if (myurl.startsWith("http")) {
					myurl.replace("http", "");
				}
				myurl = myurl + "direktanPoziv?";
				for (int i=0;i<wantedDiagnosesList.size();i++) {
					String temp = wantedDiagnosesList.get(i);
					temp = temp + "=" +temp +"&";
					myurl = myurl + temp;
				}
				if (urlPath == null) {
					rootPath = rootPath.replace('\\', '/');
					rootPath = URLEncoder.encode(rootPath, StandardCharsets.UTF_8.toString());
					myurl=myurl+"rootPath="+rootPath;
				}
				else {
					urlPath = urlPath.replace('\\', '/');
					urlPath = URLEncoder.encode(urlPath, StandardCharsets.UTF_8.toString());
					myurl=myurl+"urlPath="+urlPath;
				}
				myurl = myurl.replace(' ','+');
				myurl = myurl + "&lang=" + lang;
				LocalTime time = LocalTime.now().withNano(0);          
				LocalTime addTime = ((LocalTime) time).minusNanos(((LocalTime) timeStart).toNanoOfDay());
				timeTrack.add(addTime);
				int ukupnaMinCijena = 0;
				bitniParametri.add((String) redoslijedPozvanihParametara.get(0));
				for (int i=1;i<redoslijedPozvanihParametara.size();i++) {
					if ((double)vjerojatnostIspravno.get(i)==(double)vjerojatnostIspravno.get(i-1)) {
						redundantniParametri.add((String) redoslijedPozvanihParametara.get(i));
					}
					else {
						bitniParametri.add((String) redoslijedPozvanihParametara.get(i));  			
					}
				}    	
				for (int i=0;i<redoslijedPozvanihParametara.size();i++) {
					String temp = redoslijedPozvanihParametara.get(i);
					if (bitniParametri.contains(temp)) {
						ukupnaMinCijena = ukupnaMinCijena + redoslijedCijenaPozvanihParametara.get(i);
					}
				}
				for(int i=0; i < (timeTrack.size()-1); i++){
					LocalTime temp = ((LocalTime) timeTrack.get(i+1)).minusNanos(((LocalTime) timeTrack.get(i)).toNanoOfDay());
					timeOnParameter.add(temp);
				}		
				if (!(poslanOdgovor.equals(tocnaDijagnoza))) {
					for (int i=0;i<redoslijedPozvanihParametara.size();i++) {
						sameValuesWrongAnswer=0;
						for (int j=0;j<otherCases.size();j++) {
							temp1 = otherCases.get(j);
							for (int k=0;k<temp1.size();k++) {
								if (temp1.get(k).name.equals(redoslijedPozvanihParametara.get(i))) {
									if (!(temp1.get(k).assignedValue.equals(redoslijedVrijednostiPozvanihParametara.get(i)))) {
										otherCases.remove(j);
										otherCasesDiagnoses.remove(j);
										j--;
										break;
									}
									else {
										if ((otherCasesDiagnoses.get(j)).equals(poslanOdgovor)) {
											sameValuesWrongAnswer++;
										}
									}						
								}
							}
						}
						double temp2=(double)(sameValuesWrongAnswer)/(otherCases.size()+1)*100;
						vjerojatnostIspravnoKriviOdgovor.add(temp2);
					}
				}
				for(int i =0;i<listParameter.size();i++) {
					String temp4 = (String) listParameter.get(i);
					Parameter temp5 = LinkedListParameters.deleteByKey(list, temp4);
					nepozvaniParametri.add(temp5);
				}
				ArrayList <String> workingCasesLinkList=new ArrayList <String>();
				String workingCaseLink=new String ();
				if (workingDirectory.startsWith("http")) {
					workingCaseLink=workingDirectory+workingCase+"/case.xml";
					for (int i=0; i<diagnosisSortWantedCopy.size(); i++) {
						String temp6 = new String();
						temp6= diagnosisSortWantedCopy.get(i).caseBroj;
						temp6=workingDirectory + temp6 + "/case.xml";
						workingCasesLinkList.add(temp6);
					}
				}
				else {
					workingCaseLink="file:///"+workingDirectory+workingCase+"/case.xml";					
					workingCaseLink=workingCaseLink.replace('\\','/');
					for (int i=0; i<diagnosisSortWantedCopy.size(); i++) {
						String temp6 = new String();
						temp6= diagnosisSortWantedCopy.get(i).caseBroj;
						temp6="file:///"+workingDirectory + temp6 + "/case.xml";
						temp6=temp6.replace('\\','/');
						workingCasesLinkList.add(temp6);
					}
				}

				session.setAttribute("workingCaseLink",workingCaseLink);
				session.setAttribute("workingCasesLinkList",workingCasesLinkList);
				request.setAttribute("poslanOdgovor", poslanOdgovor);
				session.setAttribute("nevaljanaDijagnoza", nevaljanaDijagnoza);
				session.setAttribute("nepozvaniParametri", nepozvaniParametri);
				session.setAttribute("possibleSolutionsVjerojatnost", possibleSolutionsVjerojatnost);
				session.setAttribute("diagnosisSortWantedCopy", diagnosisSortWantedCopy);
				session.setAttribute("diagnosisSortWanted", diagnosisSortWanted);
				session.setAttribute("vjerojatnostIspravnoKriviOdgovor", vjerojatnostIspravnoKriviOdgovor);
				session.setAttribute("redundantniParametri", redundantniParametri);
				session.setAttribute("bitniParametri", bitniParametri);
				session.setAttribute("ukupnaMinCijena", ukupnaMinCijena);
				session.setAttribute("timeOnParameter", timeOnParameter);
				session.setAttribute("myurl", myurl);
				RequestDispatcher rd=request.getRequestDispatcher("/povratnaInformacija.jsp");
				rd.forward(request, response);
			}
		}
	}
}