package com.karlo;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Random;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

@WebServlet("/pripremiSlucajeve")

public class CaseServlet extends HttpServlet{
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();
		int brojSlucajeva = (int) session.getAttribute("brojSlucajeva");
		String rootPath = (String) session.getAttribute("rootPath");
		String urlPath = (String) session.getAttribute("urlPath");
		ArrayList<String> wantedDiagnosesList = (ArrayList<String>) session.getAttribute("wantedDiagnosesList");
		ArrayList<String> listaBezPonavljanja = (ArrayList<String>) session.getAttribute("listaBezPonavljanja"); 
		ArrayList<DiagnosisCaseNo> diagnosisSort = (ArrayList<DiagnosisCaseNo>) session.getAttribute("diagnosisSort");
		ArrayList<String> redoslijedPozvanihParametara = new ArrayList<String>();
		ArrayList<String> redoslijedVrijednostiPozvanihParametara = new ArrayList<String>();
		ArrayList<Double> vjerojatnostIspravno = new ArrayList<Double>();
		ArrayList<Integer> redoslijedCijenaPozvanihParametara = new ArrayList<Integer>();
		
		if (urlPath == null) {
			try {
				ArrayList<String> wantedCasesList = new ArrayList<String>(); //tu spremam caseove koje želim uključiti (npr. case1, case7, case25...)
				ArrayList<Parameter> listPrint = new ArrayList<Parameter>();
				ArrayList<String> listParameter = new ArrayList<String>();
				ArrayList<LocalTime> timeTrack = new ArrayList<LocalTime>();
				ArrayList<String> otherCasesDiagnoses = new ArrayList<String>(); //popis dijagnoza caseova istim redom kao otherCases
				ArrayList<ArrayList<Parameter> > otherCases = new ArrayList<ArrayList<Parameter>>();
				ArrayList<ArrayList<Parameter> > izbaceniOtherCases= new ArrayList<ArrayList<Parameter>>(); 
				ArrayList<String> izbaceniOtherCasesDiagnoses = new ArrayList<String>();
				
				String test = wantedDiagnosesList.get(0);
				if (test.equals("empty")) { // ako je u wantedDiagnosesList(0) upisan "empty" znači da se aplikacija pokreće od početka, odnosno
					// ne preko DirektanPoziv, punim listu tako da provjeravam koji su listaBezPonavljanja
					// elementi u parametrima urla
					wantedDiagnosesList.remove(0);
					for (int i=0;i<listaBezPonavljanja.size();i++) {
						String currentDiagnosis = listaBezPonavljanja.get(i);
						currentDiagnosis=request.getParameter(currentDiagnosis);
						if (currentDiagnosis!=null) {
							wantedDiagnosesList.add(currentDiagnosis);
						}
					}
				}
				if (wantedDiagnosesList.size()==0) {
					session.setAttribute("wantedDiagnosesList", wantedDiagnosesList);
			        request.getRequestDispatcher("/errorPageMissing.jsp").forward(request, response);
				}
				for (int i=0;i<diagnosisSort.size();i++) {
					if (wantedDiagnosesList.contains(diagnosisSort.get(i).dijagnoza)) {
						wantedCasesList.add(diagnosisSort.get(i).caseBroj);
					}
				}
				int randomCase = getRandomNumberInRange(0,wantedCasesList.size()-1);
				String workingCase = wantedCasesList.get(randomCase); // case# koji se rješava
				String xmlPath = rootPath + workingCase +"/case.xml";
				File fXmlFile = new File(xmlPath);
				DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
				DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
				Document doc = dBuilder.parse(fXmlFile);
				//optional, but recommended
				//read this - http://stackoverflow.com/questions/13786607/normalization-in-dom-parsing-with-java-how-does-it-work
				doc.getDocumentElement().normalize();
				NodeList nList = doc.getElementsByTagName("Parameter");
				LinkedListParameters list = new LinkedListParameters();
				for(int i=0; i<wantedCasesList.size();i++) {
					String tempCase = wantedCasesList.get(i);
					String slucajPath = rootPath +tempCase+"/case.xml";
					File fXmlFileDijagnoze = new File(slucajPath);
					DocumentBuilderFactory dbFactoryDijagnoze = DocumentBuilderFactory.newInstance();
					DocumentBuilder dBuilderDijagnoze = dbFactoryDijagnoze.newDocumentBuilder();
					Document docDijagnoze = dBuilderDijagnoze.parse(fXmlFileDijagnoze);
					docDijagnoze.getDocumentElement().normalize();
					NodeList diaList = docDijagnoze.getElementsByTagName("Diagnosis");
					Node diaNode = diaList.item(0);
					if (diaNode.getNodeType() == Node.ELEMENT_NODE) {
						Element eElement = (Element) diaNode;
						otherCasesDiagnoses.add(eElement.getElementsByTagName("Name").item(0).getTextContent());
					}
				}
				for(int i=0; i<wantedCasesList.size();i++) {
					String tempCase = wantedCasesList.get(i);
					if (!(tempCase.equals(workingCase))) {
						ArrayList<Parameter> listParameterOstali=new ArrayList<Parameter>();
						String slucajPath = rootPath +tempCase+"/case.xml";
						File fXmlFileDijagnoze = new File(slucajPath);
						DocumentBuilderFactory dbFactoryDijagnozeOstale = DocumentBuilderFactory.newInstance();
						DocumentBuilder dBuilderDijagnozeOstale = dbFactoryDijagnozeOstale.newDocumentBuilder();
						Document docDijagnozeOstale = dBuilderDijagnozeOstale.parse(fXmlFileDijagnoze);
						docDijagnozeOstale.getDocumentElement().normalize();
						NodeList diaOstaleList = docDijagnozeOstale.getElementsByTagName("Parameter");    				
						for (int temp = 0; temp < diaOstaleList.getLength(); temp++) {
							Node diaOstaleNode = diaOstaleList.item(temp);
							if (diaOstaleNode.getNodeType() == Node.ELEMENT_NODE) {
								Element eElement = (Element) diaOstaleNode;
								Parameter currentInfo = new Parameter(xmlPath, xmlPath, temp, xmlPath, xmlPath, temp, temp);
								currentInfo.name = eElement.getElementsByTagName("Name").item(0).getTextContent();
								currentInfo.synonims = eElement.getElementsByTagName("Synonims").item(0).getTextContent();
								currentInfo.availability = Integer.parseInt(eElement.getElementsByTagName("Availability").item(0).getTextContent());
								currentInfo.realValue = eElement.getElementsByTagName("RealValue").item(0).getTextContent();
								currentInfo.assignedValue = eElement.getElementsByTagName("AssignedValue").item(0).getTextContent();
								currentInfo.weight = Integer.parseInt(eElement.getElementsByTagName("Weight").item(0).getTextContent());
								currentInfo.price = Integer.parseInt(eElement.getElementsByTagName("Price").item(0).getTextContent());
								listParameterOstali.add(currentInfo);
							}
						}
						otherCases.add(listParameterOstali);
					}
				}
				for (int temp = 0; temp < nList.getLength(); temp++) {
					Node nNode = nList.item(temp);
					if (nNode.getNodeType() == Node.ELEMENT_NODE) {
						Element eElement = (Element) nNode;
						Parameter currentInfo = new Parameter(xmlPath, xmlPath, temp, xmlPath, xmlPath, temp, temp);
						currentInfo.name = eElement.getElementsByTagName("Name").item(0).getTextContent();
						currentInfo.synonims = eElement.getElementsByTagName("Synonims").item(0).getTextContent();
						currentInfo.availability = Integer.parseInt(eElement.getElementsByTagName("Availability").item(0).getTextContent());
						currentInfo.realValue = eElement.getElementsByTagName("RealValue").item(0).getTextContent();
						currentInfo.assignedValue = eElement.getElementsByTagName("AssignedValue").item(0).getTextContent();
						currentInfo.weight = Integer.parseInt(eElement.getElementsByTagName("Weight").item(0).getTextContent());
						currentInfo.price = Integer.parseInt(eElement.getElementsByTagName("Price").item(0).getTextContent());
						listParameter.add(eElement.getElementsByTagName("Name").item(0).getTextContent());
						list = LinkedListParameters.insert(list, currentInfo); 
					}
				}
				NodeList dList = doc.getElementsByTagName("Diagnosis");
				Node dNode = dList.item(0);
				if (dNode.getNodeType() == Node.ELEMENT_NODE) {
					Element eElement = (Element) dNode;
					String tocnaDijagnoza = eElement.getElementsByTagName("Name").item(0).getTextContent();
					int ukupnaCijena=0;
					Collections.sort(listParameter, String.CASE_INSENSITIVE_ORDER);
					LocalTime timeStart = LocalTime.now().withNano(0);
					otherCasesDiagnoses.remove(tocnaDijagnoza);
					ArrayList<DiagnosisCaseNo> diagnosisSortWanted= new ArrayList<DiagnosisCaseNo>();
					for (int i=0;i<diagnosisSort.size();i++) {
						if (otherCasesDiagnoses.contains(diagnosisSort.get(i).dijagnoza)) {
							diagnosisSortWanted.add(diagnosisSort.get(i));
						}
					}
					for (int i=0;i<diagnosisSortWanted.size();i++) {
						if (workingCase.equals(diagnosisSortWanted.get(i).caseBroj)) {
							diagnosisSortWanted.remove(i);
						}
					}
					
					session.setAttribute("workingCase",workingCase);
					session.setAttribute("diagnosisSortWanted", diagnosisSortWanted);
					session.setAttribute("izbaceniOtherCases", izbaceniOtherCases);
					session.setAttribute("izbaceniOtherCasesDiagnoses", izbaceniOtherCasesDiagnoses);
					session.setAttribute("redoslijedVrijednostiPozvanihParametara", redoslijedVrijednostiPozvanihParametara);
					session.setAttribute("list", list); //parametri rješavanog casea se dodaju u LinkedListParameters list 
					//pa se uklanjaju iz liste kako ih pozivamo na prikaz
					session.setAttribute("listParameter", listParameter); //String popis parametara koje caseovi nude, abecedno poredani
					session.setAttribute("listPrint", listPrint); 
					session.setAttribute("tocnaDijagnoza", tocnaDijagnoza); //String dijagnoza rješavanog casea
					session.setAttribute("timeStart", timeStart);
					session.setAttribute("timeTrack", timeTrack);
					session.setAttribute("ukupnaCijena", ukupnaCijena);
					session.setAttribute("wantedDiagnosesList", wantedDiagnosesList);//String popis svih odabranih dijagnoza abecedno sortirano bez ponavljanja
					session.setAttribute("wantedCasesList", wantedCasesList);
					session.setAttribute("otherCases", otherCases); //ostali slučajevi iz baze
					session.setAttribute("otherCasesDiagnoses", otherCasesDiagnoses); //imena ostalih slučajevi iz baze
					session.setAttribute("vjerojatnostIspravno", vjerojatnostIspravno);
					session.setAttribute("redoslijedCijenaPozvanihParametara", redoslijedCijenaPozvanihParametara);
					session.setAttribute("redoslijedPozvanihParametara", redoslijedPozvanihParametara);
					session.setAttribute("redoslijedVrijednostiPozvanihParametara", redoslijedVrijednostiPozvanihParametara);
					RequestDispatcher rd=request.getRequestDispatcher("/rjesavanjeSlucaja.jsp");
					rd.forward(request, response); 
				}
			}
			catch (Exception e){
				e.printStackTrace();
			}
		}

		else{
			try {
				ArrayList<String> wantedCasesList= new ArrayList<String>(); //tu spremam caseove koje želim uključiti (npr. case1, case7, case25...)
				ArrayList<Parameter> listPrint =new ArrayList<Parameter>();
				ArrayList<String> listParameter=new ArrayList<String>();
				ArrayList<LocalTime> timeTrack=new ArrayList<LocalTime>();
				ArrayList<String> otherCasesDiagnoses=new ArrayList<String>(); //popis dijagnoza caseova istim redom kao otherCases
				ArrayList<ArrayList<Parameter> > otherCases =  new ArrayList<ArrayList<Parameter> >();
				ArrayList<ArrayList<Parameter> > izbaceniOtherCases= new ArrayList<ArrayList<Parameter>>(); 
				ArrayList<String> izbaceniOtherCasesDiagnoses = new ArrayList<String>();
				
				String test =  wantedDiagnosesList.get(0);
				if (test.equals("empty")) {
					wantedDiagnosesList.remove(0);
					for (int i=0;i<listaBezPonavljanja.size();i++) {
						String currentDiagnosis = listaBezPonavljanja.get(i);
						currentDiagnosis=request.getParameter(currentDiagnosis);
						if (currentDiagnosis!=null) {
							wantedDiagnosesList.add(currentDiagnosis);
						}
					}
				}
				for (int i=0;i<diagnosisSort.size();i++) {
					if (wantedDiagnosesList.contains(diagnosisSort.get(i).dijagnoza)) {
						wantedCasesList.add(diagnosisSort.get(i).caseBroj);
					}
				}
				if (wantedDiagnosesList.size()==0) {
					session.setAttribute("wantedDiagnosesList", wantedDiagnosesList);
			        request.getRequestDispatcher("/errorPageMissing.jsp").forward(request, response);
				}
				int randomCase = getRandomNumberInRange(0,wantedCasesList.size()-1);
				String workingCase = wantedCasesList.get(randomCase); // case# koji se rješava
				String slucajPath = urlPath + workingCase +"/case.xml";
				String tempPath = "urlBaza/temp.xml";
				URL url = new URL(slucajPath);
				File file = new File(tempPath);
				org.apache.commons.io.FileUtils.copyURLToFile(url, file);
				String xmlPath = tempPath;
				File fXmlFile = new File(xmlPath);
				DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
				DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
				Document doc = dBuilder.parse(fXmlFile);
				doc.getDocumentElement().normalize();
				NodeList nList = doc.getElementsByTagName("Parameter");
				LinkedListParameters list = new LinkedListParameters(); //nova baza slučajeva, odnosno izabrani slučajevi koji će se obrađivati    	
				for(int i=0; i<wantedCasesList.size();i++) {
					String tempCase = wantedCasesList.get(i);
					String tempSlucajPath = urlPath +tempCase+"/case.xml";
					URL url1 = new URL(tempSlucajPath);
					org.apache.commons.io.FileUtils.copyURLToFile(url1, file);
					File fXmlFileDijagnoze = new File(tempPath);
					DocumentBuilderFactory dbFactoryDijagnoze = DocumentBuilderFactory.newInstance();
					DocumentBuilder dBuilderDijagnoze = dbFactoryDijagnoze.newDocumentBuilder();
					Document docDijagnoze = dBuilderDijagnoze.parse(fXmlFileDijagnoze);
					docDijagnoze.getDocumentElement().normalize();
					NodeList diaList = docDijagnoze.getElementsByTagName("Diagnosis");
					Node diaNode = diaList.item(0);
					if (diaNode.getNodeType() == Node.ELEMENT_NODE) {
						Element eElement = (Element) diaNode;
						otherCasesDiagnoses.add(eElement.getElementsByTagName("Name").item(0).getTextContent());
					}
				}
				for(int i=0; i<wantedCasesList.size();i++) {
					String tempCase = wantedCasesList.get(i);
					if (!(tempCase.equals(workingCase))) {
						ArrayList<Parameter> listParameterOstali=new ArrayList<Parameter>();
						String tempSlucajPath = urlPath +tempCase+"/case.xml";
						URL url1 = new URL(tempSlucajPath);
						org.apache.commons.io.FileUtils.copyURLToFile(url1, file);
						File fXmlFileDijagnoze = new File(tempPath);
						DocumentBuilderFactory dbFactoryDijagnozeOstale = DocumentBuilderFactory.newInstance();
						DocumentBuilder dBuilderDijagnozeOstale = dbFactoryDijagnozeOstale.newDocumentBuilder();
						Document docDijagnozeOstale = dBuilderDijagnozeOstale.parse(fXmlFileDijagnoze);
						docDijagnozeOstale.getDocumentElement().normalize();
						NodeList diaOstaleList = docDijagnozeOstale.getElementsByTagName("Parameter");    				
						for (int temp = 0; temp < diaOstaleList.getLength(); temp++) {
							Node diaOstaleNode = diaOstaleList.item(temp);
							if (diaOstaleNode.getNodeType() == Node.ELEMENT_NODE) {
								Element eElement = (Element) diaOstaleNode;
								Parameter currentInfo = new Parameter(xmlPath, xmlPath, temp, xmlPath, xmlPath, temp, temp);
								currentInfo.name = eElement.getElementsByTagName("Name").item(0).getTextContent();
								currentInfo.synonims = eElement.getElementsByTagName("Synonims").item(0).getTextContent();
								currentInfo.availability = Integer.parseInt(eElement.getElementsByTagName("Availability").item(0).getTextContent());
								currentInfo.realValue = eElement.getElementsByTagName("RealValue").item(0).getTextContent();
								currentInfo.assignedValue = eElement.getElementsByTagName("AssignedValue").item(0).getTextContent();
								currentInfo.weight = Integer.parseInt(eElement.getElementsByTagName("Weight").item(0).getTextContent());
								currentInfo.price = Integer.parseInt(eElement.getElementsByTagName("Price").item(0).getTextContent());
								listParameterOstali.add(currentInfo);
							}
						}
						otherCases.add(listParameterOstali);
					}
				}
				for (int temp = 0; temp < nList.getLength(); temp++) {
					Node nNode = nList.item(temp);
					if (nNode.getNodeType() == Node.ELEMENT_NODE) {
						Element eElement = (Element) nNode;
						Parameter currentInfo = new Parameter(xmlPath, xmlPath, temp, xmlPath, xmlPath, temp, temp);
						currentInfo.name = eElement.getElementsByTagName("Name").item(0).getTextContent();
						currentInfo.synonims = eElement.getElementsByTagName("Synonims").item(0).getTextContent();
						currentInfo.availability = Integer.parseInt(eElement.getElementsByTagName("Availability").item(0).getTextContent());
						currentInfo.realValue = eElement.getElementsByTagName("RealValue").item(0).getTextContent();
						currentInfo.assignedValue = eElement.getElementsByTagName("AssignedValue").item(0).getTextContent();
						currentInfo.weight = Integer.parseInt(eElement.getElementsByTagName("Weight").item(0).getTextContent());
						currentInfo.price = Integer.parseInt(eElement.getElementsByTagName("Price").item(0).getTextContent());
						listParameter.add(eElement.getElementsByTagName("Name").item(0).getTextContent());
						list = LinkedListParameters.insert(list, currentInfo); 
					}
				}
				NodeList dList = doc.getElementsByTagName("Diagnosis");
				Node dNode = dList.item(0);
				if (dNode.getNodeType() == Node.ELEMENT_NODE) {
					Element eElement = (Element) dNode;
					String tocnaDijagnoza = eElement.getElementsByTagName("Name").item(0).getTextContent();
					int ukupnaCijena=0;
					Collections.sort(listParameter, String.CASE_INSENSITIVE_ORDER);
					otherCasesDiagnoses.remove(tocnaDijagnoza);
					LocalTime timeStart = LocalTime.now().withNano(0);
					ArrayList<DiagnosisCaseNo> diagnosisSortWanted= new ArrayList<DiagnosisCaseNo>();
					for (int i=0;i<diagnosisSort.size();i++) {
						if (otherCasesDiagnoses.contains(diagnosisSort.get(i).dijagnoza)) {
							diagnosisSortWanted.add(diagnosisSort.get(i));
						}
					}
					for (int i=0;i<diagnosisSortWanted.size();i++) {
						if (workingCase.equals(diagnosisSortWanted.get(i).caseBroj)) {
							diagnosisSortWanted.remove(i);
						}
					}
					
					session.setAttribute("workingCase",workingCase);
					session.setAttribute("diagnosisSortWanted", diagnosisSortWanted);
					session.setAttribute("izbaceniOtherCases", izbaceniOtherCases);
					session.setAttribute("izbaceniOtherCasesDiagnoses", izbaceniOtherCasesDiagnoses);
					session.setAttribute("redoslijedVrijednostiPozvanihParametara", redoslijedVrijednostiPozvanihParametara);
					session.setAttribute("list", list); //parametri rješavanog casea se dodaju u LinkedListParameters list 
					//pa se lista kako pozivamo parametre
					session.setAttribute("listParameter", listParameter); //String popis parametara koje caseovi nude, abecedno poredani
					session.setAttribute("listPrint", listPrint); 
					session.setAttribute("tocnaDijagnoza", tocnaDijagnoza); //String dijagnoza rješavanog casea
					session.setAttribute("timeTrack", timeTrack);
					session.setAttribute("timeStart", timeStart);
					session.setAttribute("ukupnaCijena", ukupnaCijena);
					session.setAttribute("wantedDiagnosesList", wantedDiagnosesList);//String popis svih odabranih dijagnoza abecedno sortirano bez ponavljanja
					session.setAttribute("wantedCasesList", wantedCasesList);
					session.setAttribute("redoslijedPozvanihParametara", redoslijedPozvanihParametara);
					session.setAttribute("vjerojatnostIspravno", vjerojatnostIspravno);
					session.setAttribute("otherCases", otherCases); //ostali slučajevi iz baze
					session.setAttribute("otherCasesDiagnoses", otherCasesDiagnoses); //imena ostalih slučajevi iz baze
					session.setAttribute("redoslijedCijenaPozvanihParametara", redoslijedCijenaPozvanihParametara);
					session.setAttribute("redoslijedVrijednostiPozvanihParametara", redoslijedVrijednostiPozvanihParametara);
					RequestDispatcher rd=request.getRequestDispatcher("/rjesavanjeSlucaja.jsp");
					rd.forward(request, response); 
				}
			}
			catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	private static int getRandomNumberInRange(int min, int max) {
		Random r = new Random();
		return r.ints(min, (max + 1)).limit(1).findFirst().getAsInt();
	}    
}