package com.karlo;

public class Parameter{
	
    public String name; 
    public String synonims;  
    public int availability;
    public String realValue;
    public String assignedValue;
    public int weight;
    public int price;
    
	public Parameter(String name, String synonims, int availability, String realValue, String assignedValue, int weight,
			int price) {
		super();
		this.name = name;
		this.synonims = synonims;
		this.availability = availability;
		this.realValue = realValue;
		this.assignedValue = assignedValue;
		this.weight = weight;
		this.price = price;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSynonims() {
		return synonims;
	}

	public void setSynonims(String synonims) {
		this.synonims = synonims;
	}

	public int getAvailability() {
		return availability;
	}

	public void setAvailability(int availability) {
		this.availability = availability;
	}

	public String getRealValue() {
		return realValue;
	}

	public void setRealValue(String realValue) {
		this.realValue = realValue;
	}

	public String getAssignedValue() {
		return assignedValue;
	}

	public void setAssignedValue(String assignedValue) {
		this.assignedValue = assignedValue;
	}

	public int getWeight() {
		return weight;
	}

	public void setWeight(int weight) {
		this.weight = weight;
	}

	public int getPrice() {
		return price;
	}

	public void setPrice(int price) {
		this.price = price;
	}

	@Override
	public String toString() {
		return "Parameter [name=" + name + ", synonims=" + synonims + ", availability=" + availability + ", realValue="
				+ realValue + ", assignedValue=" + assignedValue + ", weight=" + weight + ", price=" + price + "]";
	}    
 }