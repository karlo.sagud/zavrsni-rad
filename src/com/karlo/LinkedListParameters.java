package com.karlo;

import java.util.Objects;
 		
public class LinkedListParameters {

	Node head; // head of list

	static class Node {

		Parameter data;
		Node next;

		// Constructor
		Node(Parameter d) {
			data = d;
			next = null;
		}
	}

	public static LinkedListParameters insert(LinkedListParameters list, Parameter data) {
		// Create a new node with given data
		Node new_node = new Node(data);
		new_node.next = null;

		// If the Linked List is empty,
		// then make the new node as head
		if (list.head == null) {
			list.head = new_node;
		} else {
			// Else traverse till the last node
			// and insert the new_node there
			Node last = list.head;
			while (last.next != null) {
				last = last.next;
			}

			// Insert the new_node at last node
			last.next = new_node;
		}

		// Return the list by head
		return list;
	}

	public static void printList(LinkedListParameters list) {
		Node currNode = list.head;

		System.out.print("\nLinkedList: ");

		// Traverse through the LinkedList
		while (currNode != null) {
			// Print the data at current node
			System.out.print(currNode.data.name + " ");
			System.out.print(currNode.data.realValue + " ");
			System.out.println("\n");
			// Go to next node
			currNode = currNode.next;
		}
		System.out.println("\n");
	}
	
	public static Parameter returnListElement(LinkedListParameters list) {
		Node currNode = list.head;

		System.out.print("\nLinkedList: ");

		// Traverse through the LinkedList
		while (currNode != null) {
			// Print the data at current node
			System.out.print(currNode.data.name + " ");
			System.out.print(currNode.data.realValue + " ");
			System.out.println("\n");
			// Go to next node
			currNode = currNode.next;
		}
		System.out.println("\n");
		return currNode.data;
	}

	public static Parameter deleteByKey(LinkedListParameters list, String key) {
		// Store head node
		Parameter izbaceniPodatak = new Parameter(key, key, 0, key, key, 0, 0);

		Node currNode = list.head, prev = null;
		
		izbaceniPodatak = currNode.data;
		
		//
		// CASE 1:
		// If head node itself holds the key to be deleted

		if (currNode != null && Objects.equals(currNode.data.name, key)) {
			list.head = currNode.next; // Changed head
			return izbaceniPodatak;
		}
		while (currNode != null && !Objects.equals(currNode.data.name, key)) {
			// If currNode does not hold key
			// continue to next node
			prev = currNode;
			currNode = currNode.next;
		}
		// If the key was present, it should be at currNode
		// Therefore the currNode shall not be null
		if (currNode != null) {
			// Since the key is at currNode
			// Unlink currNode from linked list
			izbaceniPodatak = currNode.data;
			prev.next = currNode.next;
			
			// Display the message
			//System.out.println(key + " found and deleted");
		}

		if (currNode == null) {
			// Display the message
			//System.out.println(key + " not found");
			izbaceniPodatak = null;
		}

		// return the List
		return izbaceniPodatak;
	}

	public static LinkedListParameters deleteAtPosition(LinkedListParameters list, int index) {
		// Store head node
		Node currNode = list.head, prev = null;
		if (index == 0 && currNode != null) {
			list.head = currNode.next; // Changed head

			// Return the updated List
			return list;
		}
		int counter = 0;
		while (currNode != null) {

			if (counter == index) {
				// Since the currNode is the required position
				// Unlink currNode from linked list
				prev.next = currNode.next;

				// Display the message
				break;
			} else {
				// If current position is not the index
				// continue to next node
				prev = currNode;
				currNode = currNode.next;
				counter++;
			}
		}
		if (currNode == null) {
			// Display the message
		}
		// return the List
		return list;
	}
}