package com.karlo;

public class DiagnosisCaseNo {	
    public String dijagnoza;
    public String caseBroj;
	
    public DiagnosisCaseNo(String dijagnoza, String caseBroj) {
    	super();
		this.dijagnoza = dijagnoza;
		this.caseBroj = caseBroj;
	}

	public String getDijagnoza() {
		return dijagnoza;
	}

	public void setDijagnoza(String dijagnoza) {
		this.dijagnoza = dijagnoza;
	}

	public String getCaseBroj() {
		return caseBroj;
	}

	public void setCaseBroj(String caseBroj) {
		this.caseBroj = caseBroj;
	}
	
	@Override
	public String toString() {
		return "DiagnosisCaseNo [dijagnoza=" + dijagnoza + ", caseBroj=" + caseBroj + "]";
	}
}