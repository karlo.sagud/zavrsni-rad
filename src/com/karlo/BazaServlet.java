package com.karlo;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Objects;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.DocumentBuilder;
import org.jsoup.Jsoup;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;
import org.w3c.dom.Node;
import org.w3c.dom.Element;
import java.io.*;
import java.net.URL;

@WebServlet("/otvoriBazu")

public class BazaServlet extends HttpServlet {
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();
		String urlPath = request.getParameter("urlPath"); //url baze
		String rootPath = request.getParameter("rootPath");	//adresa baze na racunalu
		String lang = request.getParameter("lang");//jezik
		ArrayList<String> wantedDiagnosesList = new ArrayList<String>(); //lista dijagnoza koje smo odabrali za rješavanje
		ArrayList<Integer> brojSlucajevaPoDijagnozi = new ArrayList<Integer>();
		String error = new String(); // string koji opisuje grešku
		session.setAttribute("lang", lang);

		if (urlPath == null) {  //baza na disku
			if(!(Objects.equals(rootPath.substring(rootPath.length()-1),"\\") || Objects.equals(rootPath.substring(rootPath.length()-1),"//"))) {
				rootPath = rootPath+"/";
			} //dodavanje /na kraj patha ako nedostaje
			try {
				File file = new File(rootPath);
				File[] files = file.listFiles(new FileFilter() {
					@Override
					public boolean accept(File f) {
						return f.isDirectory();
					}
				}); //brojanje caseova u bazi
				int brojSlucajeva = files.length;
				if (brojSlucajeva==0) {
					error = "errorRoot";
					session.setAttribute("error", error);
					session.setAttribute("rootPath", rootPath);
					request.getRequestDispatcher("/odabirBaze.jsp").forward(request, response);
				}
				try {
					ArrayList<DiagnosisCaseNo> diagnosisSort= new ArrayList<DiagnosisCaseNo>(); //lista u kojoj su zapisani
					//u parovima dijagnoza i broj casea
					for(int i=1; i<=brojSlucajeva;i++) {
						String slucajPath = rootPath +"case"+ i +"/case.xml";
						DiagnosisCaseNo tempCase = new DiagnosisCaseNo(rootPath, rootPath);
						File fXmlFileDijagnoze = new File(slucajPath);
						if(!(fXmlFileDijagnoze.exists() && !fXmlFileDijagnoze.isDirectory())) {
							error = "errorRoot";
							session.setAttribute("error", error);
							session.setAttribute("rootPath", rootPath);
							request.getRequestDispatcher("/odabirBaze.jsp").forward(request, response);
						}
						DocumentBuilderFactory dbFactoryDijagnoze = DocumentBuilderFactory.newInstance();
						DocumentBuilder dBuilderDijagnoze = dbFactoryDijagnoze.newDocumentBuilder();
						Document docDijagnoze = dBuilderDijagnoze.parse(fXmlFileDijagnoze);
						docDijagnoze.getDocumentElement().normalize();
						NodeList diaList = docDijagnoze.getElementsByTagName("Diagnosis");        			      
						Node diaNode = diaList.item(0);
						if (diaNode.getNodeType() == Node.ELEMENT_NODE) {
							Element eElement = (Element) diaNode;
							tempCase.dijagnoza = eElement.getElementsByTagName("Name").item(0).getTextContent();
							tempCase.caseBroj = "case" + i;
							diagnosisSort.add(tempCase); //sve dijagnoze u bazi uparane s respektivnim caseovima
							// kasnije sortirane abecedno
						}
					}
					Collections.sort(diagnosisSort, Comparator.comparing(DiagnosisCaseNo::getDijagnoza));
					ArrayList<String> listaBezPonavljanja= new ArrayList<String>(); //lista u kojoj su zapisane 
					//dijagnoze, ali svaka najviše jedanput
					for(int i=0; i < diagnosisSort.size(); i++) {
						if(( listaBezPonavljanja.contains(diagnosisSort.get(i).dijagnoza))) {}
						else 
							listaBezPonavljanja.add(diagnosisSort.get(i).dijagnoza);				
					}
					int brojac = 0;
					for(int i=0;i<(diagnosisSort.size()-1);i++) {
						brojac++;
						if (!(diagnosisSort.get(i).dijagnoza).equals(diagnosisSort.get(i+1).dijagnoza)){
							brojSlucajevaPoDijagnozi.add(brojac);
							brojac = 0;
						}
					}
					brojac++;
					brojSlucajevaPoDijagnozi.add(brojac);
					wantedDiagnosesList.add("empty");
					String workingDirectory = new String(rootPath); // lokacija baze na disku ili urlu, koristi se kod feedbacka

					session.setAttribute("workingDirectory", workingDirectory);
					session.setAttribute("brojSlucajevaPoDijagnozi", brojSlucajevaPoDijagnozi);
					session.setAttribute("brojSlucajeva", brojSlucajeva);
					session.setAttribute("diagnosisSort", diagnosisSort); //lista svih (dijagnoza + caseNumber)
					session.setAttribute("rootPath", rootPath);
					session.setAttribute("urlPath", urlPath);
					session.setAttribute("listaBezPonavljanja", listaBezPonavljanja); //lista svih dijagnoza ali bez ponavljanja
					session.setAttribute("wantedDiagnosesList", wantedDiagnosesList);
					RequestDispatcher rd=request.getRequestDispatcher("odabirDijagnoza.jsp");
					rd.forward(request, response); 
				}	
				catch(Exception e) {}		
			}
			catch(Exception e){
				error = "errorRoot";
				session.setAttribute("error", error);
				session.setAttribute("rootPath", rootPath);
				request.getRequestDispatcher("/odabirBaze.jsp").forward(request, response);
			}
		}

		else { //baza online
			try {
				urlPath = urlPath.trim();
				int	brojSlucajeva=-1;
				int index = 0;
				if (!(urlPath.startsWith("http"))) {
					error = "errorURL";
					session.setAttribute("error", error);
					session.setAttribute("urlPath", urlPath);
					request.getRequestDispatcher("/odabirBaze.jsp").forward(request, response);
				}
				String html = Jsoup.connect(urlPath).get().html();					
				String tempPath = "urlBaza/temp.xml";
				ArrayList<DiagnosisCaseNo> diagnosisSort= new ArrayList<DiagnosisCaseNo>();				
				do {
					brojSlucajeva++;
					index = html.indexOf("[DIR]",index+1);
				} while(index!=-1);   			 //brojanje slučajeva
				if (brojSlucajeva==0) {
					error = "errorURL";
					session.setAttribute("error", error);
					session.setAttribute("urlPath", urlPath);
					request.getRequestDispatcher("/odabirBaze.jsp").forward(request, response);
				}
				for(int i=1; i<=brojSlucajeva;i++) { //čitanje dijagnoza u slučajevima
					String slucajPath = urlPath +"case"+ i +"/case.xml";
					URL url = new URL(slucajPath);
					File file = new File(tempPath);
					try {
						org.apache.commons.io.FileUtils.copyURLToFile(url, file);
					}
					catch(Exception e){
						error = "errorURL";
						session.setAttribute("error", error);
						session.setAttribute("urlPath", urlPath);
						request.getRequestDispatcher("/odabirBaze.jsp").forward(request, response);					
					}
					DiagnosisCaseNo tempCase = new DiagnosisCaseNo(tempPath, tempPath);
					File fXmlFileDijagnoze = new File(tempPath);
					DocumentBuilderFactory dbFactoryDijagnoze = DocumentBuilderFactory.newInstance();
					DocumentBuilder dBuilderDijagnoze = dbFactoryDijagnoze.newDocumentBuilder();
					Document docDijagnoze = dBuilderDijagnoze.parse(fXmlFileDijagnoze);
					docDijagnoze.getDocumentElement().normalize();
					NodeList diaList = docDijagnoze.getElementsByTagName("Diagnosis");        			      
					Node diaNode = diaList.item(0);
					if (diaNode.getNodeType() == Node.ELEMENT_NODE) {
						Element eElement = (Element) diaNode;
						tempCase.dijagnoza = eElement.getElementsByTagName("Name").item(0).getTextContent();
						tempCase.caseBroj = "case" + i;
						diagnosisSort.add(tempCase);
					}
				}
				Collections.sort(diagnosisSort, Comparator.comparing(DiagnosisCaseNo::getDijagnoza));
				ArrayList<String> listaBezPonavljanja= new ArrayList<String>(); //lista u kojoj su zapisane
				//dijagnoze, ali svaka najviše jedanput
				for(int i=0; i < diagnosisSort.size(); i++){
					if(( listaBezPonavljanja.contains(diagnosisSort.get(i).dijagnoza))) {}
					else 
						listaBezPonavljanja.add(diagnosisSort.get(i).dijagnoza);					
				}
				int brojac = 0;
				for(int i=0;i<(diagnosisSort.size()-1);i++) {
					brojac++;
					if (!(diagnosisSort.get(i).dijagnoza).equals(diagnosisSort.get(i+1).dijagnoza)){
						brojSlucajevaPoDijagnozi.add(brojac);
						brojac = 0;
					}
				}
				brojac++;
				brojSlucajevaPoDijagnozi.add(brojac);
				wantedDiagnosesList.add("empty");
				String workingDirectory = new String(urlPath); // lokacija baze na disku ili urlu, koristi se kod feedbacka

				session.setAttribute("workingDirectory", workingDirectory);				
				session.setAttribute("brojSlucajevaPoDijagnozi", brojSlucajevaPoDijagnozi);
				session.setAttribute("brojSlucajeva", brojSlucajeva);
				session.setAttribute("diagnosisSort", diagnosisSort); //lista svih (dijagnoza + caseNumber)
				session.setAttribute("rootPath", rootPath);
				session.setAttribute("urlPath", urlPath);
				session.setAttribute("listaBezPonavljanja", listaBezPonavljanja); //lista svih dijagnoza ali bez ponavljanja
				session.setAttribute("wantedDiagnosesList", wantedDiagnosesList);
				RequestDispatcher rd=request.getRequestDispatcher("odabirDijagnoza.jsp");
				rd.forward(request, response); 
			}
			catch(IOException ex) {
				ex.printStackTrace();
			} catch (SAXException e) {
				e.printStackTrace();
			} catch (ParserConfigurationException e) {
				e.printStackTrace();
			}
		}
	}
}