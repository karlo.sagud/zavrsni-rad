package com.karlo;

import java.io.IOException;
import java.time.LocalTime;
import java.util.ArrayList;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

@WebServlet("/obrada")

public class ObradaServlet extends HttpServlet {
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session=request.getSession();
		Parameter izbaceniPodatak = new Parameter(null, null, 0, null, null, 0, 0);
		LinkedListParameters list=(LinkedListParameters) session.getAttribute("list");
		int ukupnaCijena= (int) session.getAttribute("ukupnaCijena");
		ArrayList listPrint =(ArrayList) session.getAttribute("listPrint");
		ArrayList timeTrack = (ArrayList) session.getAttribute("timeTrack");
		ArrayList listParameter = (ArrayList) session.getAttribute("listParameter");
		ArrayList redoslijedCijenaPozvanihParametara = (ArrayList) session.getAttribute("redoslijedCijenaPozvanihParametara");
		ArrayList redoslijedPozvanihParametara = (ArrayList) session.getAttribute("redoslijedPozvanihParametara");
		ArrayList redoslijedVrijednostiPozvanihParametara = (ArrayList) session.getAttribute("redoslijedVrijednostiPozvanihParametara");
		LocalTime timeStart = (LocalTime) session.getAttribute("timeStart");
		ArrayList <ArrayList<Parameter>> otherCases = (ArrayList<ArrayList<Parameter>>) session.getAttribute("otherCases");
		ArrayList<DiagnosisCaseNo> diagnosisSortWanted = (ArrayList<DiagnosisCaseNo>) session.getAttribute("diagnosisSortWanted");
		ArrayList otherCasesDiagnoses = (ArrayList) session.getAttribute("otherCasesDiagnoses");
		ArrayList<Double> vjerojatnostIspravno = (ArrayList<Double>) session.getAttribute("vjerojatnostIspravno");
		String tocnaDijagnoza = (String) session.getAttribute("tocnaDijagnoza");
		ArrayList <ArrayList<Parameter>> otherCasesWrongDiagnosis = (ArrayList<ArrayList<Parameter>>) session.getAttribute("otherCasesWrongDiagnosis");
		ArrayList<String> otherCasesDiagnosesWrongDiagnosis = (ArrayList<String>) session.getAttribute("otherCasesDiagnosesWrongDiagnosis");
		ArrayList <ArrayList<Parameter>> izbaceniOtherCases = (ArrayList<ArrayList<Parameter>>) session.getAttribute("izbaceniOtherCases");
		ArrayList<String> izbaceniOtherCasesDiagnoses = (ArrayList) session.getAttribute("izbaceniOtherCasesDiagnoses");
		String ukloniPodatak = request.getParameter("podatak");
		ArrayList <Parameter> temp = new ArrayList <Parameter>();
		ArrayList<ArrayList<Parameter>> dodajUOtherCases = new ArrayList<ArrayList<Parameter>>();
		String errorP = new String(); // string koji opisuje grešku 

		ukloniPodatak = ukloniPodatak.substring(0,1).toUpperCase() + ukloniPodatak.substring(1).toLowerCase();
		izbaceniPodatak=LinkedListParameters.deleteByKey(list, ukloniPodatak);
		if(izbaceniPodatak!=null) {
			redoslijedPozvanihParametara.add(ukloniPodatak);
			int sameValuesSameDiagnosis = 0;
			int sameValuesAnyDiagnosis = 0;
			for (int i=0;i<otherCases.size();i++) {
				temp = otherCases.get(i);
				for (int j=0;j<temp.size();j++) {
					if (temp.get(j).name.equals(izbaceniPodatak.name)) {
						if (!(temp.get(j).assignedValue.equals(izbaceniPodatak.assignedValue))) {
							izbaceniOtherCases.add(otherCases.get(i));
							izbaceniOtherCasesDiagnoses.add((String) otherCasesDiagnoses.get(i));						
							otherCases.remove(i);
							otherCasesDiagnoses.remove(i);
							diagnosisSortWanted.remove(i);
							i--;
							break;
						}
						else {
							if ((otherCasesDiagnoses.get(i)).equals(tocnaDijagnoza)) {
								sameValuesSameDiagnosis++;
							}
						}
					}
				}
			}
			redoslijedVrijednostiPozvanihParametara.add(izbaceniPodatak.assignedValue);
			redoslijedCijenaPozvanihParametara.add(izbaceniPodatak.price);
			sameValuesAnyDiagnosis = otherCases.size();
			double vjerojatnostTocnosti=(double)(sameValuesSameDiagnosis+1)/(sameValuesAnyDiagnosis+1)*100; //+1 za case koji se rješava
			vjerojatnostIspravno.add(vjerojatnostTocnosti);
			listParameter.remove(new String(ukloniPodatak)); 
			ukupnaCijena=ukupnaCijena + izbaceniPodatak.price;
			listPrint.add(izbaceniPodatak);
			LocalTime time = LocalTime.now().withNano(0);  
			LocalTime addTime = ((LocalTime) time).minusNanos(((LocalTime) timeStart).toNanoOfDay());
			timeTrack.add(addTime);		
		}		
		else {
			errorP="errorParameter";
			session.setAttribute("ukloniPodatak", ukloniPodatak);
			session.setAttribute("errorP", errorP);
			request.getRequestDispatcher("/rjesavanjeSlucaja.jsp").forward(request, response);
		}

		session.setAttribute("diagnosisSortWanted", diagnosisSortWanted);
		session.setAttribute("redoslijedVrijednostiPozvanihParametara", redoslijedVrijednostiPozvanihParametara);
		session.setAttribute("izbaceniOtherCases", izbaceniOtherCases);
		session.setAttribute("izbaceniOtherCasesDiagnoses", izbaceniOtherCasesDiagnoses);
		session.setAttribute("otherCases", otherCases);
		session.setAttribute("otherCasesDiagnoses", otherCasesDiagnoses);
		session.setAttribute("redoslijedCijenaPozvanihParametara", redoslijedCijenaPozvanihParametara);
		session.setAttribute("redoslijedPozvanihParametara", redoslijedPozvanihParametara);
		session.setAttribute("vjerojatnostIspravno", vjerojatnostIspravno);
		session.setAttribute("listParameter",listParameter);
		session.setAttribute("timeTrack", timeTrack);
		request.setAttribute("listPrint", listPrint);
		session.setAttribute("ukupnaCijena",ukupnaCijena);
		session.setAttribute("timeStart", timeStart);
		RequestDispatcher rd=request.getRequestDispatcher("/rjesavanjeSlucaja.jsp");
		rd.forward(request, response); 
	}
}