<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8" isELIgnored="false" errorPage="ErrorPage.jsp"%> 

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>

<!DOCTYPE html>
<fmt:setLocale value="${lang}" />
<fmt:setBundle basename="language.messages" />

<html>
<head>

<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" type="text/css" href="//fonts.googleapis.com/css?family=Open+Sans" />
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/2.1.0/jquery.min.js"></script>

<style>
.collapsible {
	background-color: #00819c;
	color: #ffffff;
	cursor: pointer;
	padding: 6px;
	padding-left: 18px;
	width: 100%;
	border: 1px solid #006b86;
	text-align: left;
	outline: none;
	font-size: 16px;
	border-radius: 4px;	
}

.content {
	display: block;
	padding: 5px 18px;
	overflow: hidden;
	transition: max-height 0.1s ease-out;
	background-color: #f1f1f1;
}

.gumb {
  background-color: #5bc0de;
  border: none;
  color: white;
  padding: 2px;
  text-decoration: none;
  border: 1px solid #46b8da;
  cursor: pointer;
  border-radius: 4px;
  margin-top: 10px;
}

.gumb:hover {
	background-color: #46b8da;
}

html * {
	font-size: 16px !important;
	color: #000;
	font-family: Open Sans !important;
	font-weight: normal;
}
</style>
<title><fmt:message key="title2"/></title>
</head>
<body>
	<button type="button" class="collapsible"><fmt:message key="biranjedijagnoza"/></button>
	<div class="content">
	<fmt:message key="odaberidijagnoze"/>
		<form action="pripremiSlucajeve" method="get">
			<c:forEach var="element" items="${listaBezPonavljanja}" varStatus="status">
				<input type="checkbox" id="${element}" name="${element}"
					value="${element}" checked >
				<label for="${element}"><span style="font-weight: normal">${element}</span></label>
				<td>[<c:out value="${brojSlucajevaPoDijagnozi[status.index]}"/>]</td>
				<br>
			</c:forEach>
			<fmt:message key="totalcases"/>: <c:out value="${brojSlucajeva}"/>
			<br>		
			<input class="gumb" type="submit"  value="<fmt:message key="confirm"/>" id="checkBtn">
		</form>
	</div>
	<script>
		sessionStorage.setItem("ukupnoVrijeme", 0);
	</script>
	
	<script type="text/javascript">
$(document).ready(function () {
    $('#checkBtn').click(function() {
      checked = $("input[type=checkbox]:checked").length;

      if(!checked) {
        return false;
      }

    });
});

</script>
</body>
</html>