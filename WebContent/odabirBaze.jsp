<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8" isELIgnored="false" errorPage="ErrorPage.jsp"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

<!DOCTYPE html>
<c:choose>
	<c:when test = "${param.l == 'en' || param.l == 'hr'}">
		<fmt:setLocale value="${param.l}" scope="session"/>
	</c:when>
	<c:otherwise>
		<fmt:setLocale value="${lang}" scope="session"/>
	</c:otherwise>
</c:choose>	
<fmt:setBundle basename="language.messages" scope="session"/>
<html>
<head>

<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" type="text/css"
	href="//fonts.googleapis.com/css?family=Open+Sans" />
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
	
<style>
form {
    display: inline-block; 
}

.collapsible {
	background-color: #00819c;
	color: #ffffff;
	cursor: pointer;
	padding: 6px;
	padding-left: 18px;
	width: 100%;
	border: 1px solid #006b86;
	text-align: left;
	outline: none;
	font-size: 16px;
	border-radius: 4px;
}

.content {
	display: block;
	padding: 5px 18px;
	overflow: hidden;
	transition: max-height 0.1s ease-out;
	background-color: #f1f1f1;
}

.gumb {
  background-color: #5bc0de;
  border: none;
  color: white;
  padding: 2px;
  text-decoration: none;
  border: 1px solid #46b8da;
  margin: 2px 2px;
  cursor: pointer;
  border-radius: 4px;
}

.gumb:hover {
	background-color: #46b8da;
}

html * {
	font-size: 16px !important;
	color: #000;
	font-family: Open Sans !important;
	font-weight: normal;
}
</style>

<title><fmt:message key="title1"/></title>

</head>

<body>

	<button type="button" class="collapsible"><fmt:message key="biranjebaze"/>
	<form style="float: right; padding-left: 8px;padding-right: 10px;" action="odabirBaze.jsp" method="post">
  <input type="hidden" name="l" value="hr">
  <input type="image" src="/DSAT/images/hr.png">
</form>
	<form style="float: right;" action="odabirBaze.jsp" method="post">
  	<input type="hidden" name="l" value="en">
 	 <input type="image" src="/DSAT/images/en.png">
	</form>
	
</button>
	<div class="content">
		<form action="otvoriBazu">
		<c:choose>
			<c:when test = "${param.l == 'en' || param.l == 'hr'}">
				<input type="hidden" name="lang" value="${param.l}">
			</c:when>
			<c:otherwise>
				<input type="hidden" name="lang" value="${lang}">
			</c:otherwise>
			</c:choose>			
			<div>
				<label><fmt:message key="bazadisk"/></label> <input
					type="text" name="rootPath" placeholder="<fmt:message key="localpath"/>" required> <input class="gumb" type="submit"  value="<fmt:message key="submit"/>">
			<c:if test = "${error == 'errorRoot'}"> <p style="color:red"><fmt:message key="errorlocalbasetoserver"/></p>
			<c:set var="error" value="${rootPath}" scope="session"/>
		
			</c:if>
			</div>
		</form>
		<br>
		<form action="otvoriBazu">
		<c:choose>
			<c:when test = "${param.l == 'en' || param.l == 'hr'}">
				<input type="hidden" name="lang" value="${param.l}">
			</c:when>
			<c:otherwise>
				<input type="hidden" name="lang" value="${lang}">
			</c:otherwise>
			</c:choose>	
			<div>
				<label><fmt:message key="bazaurl"/></label> <input type="text"
					name="urlPath" placeholder="<fmt:message key="urlpath"/>" required> <input class="gumb" type="submit"  value="<fmt:message key="submit"/>">
						<c:if test = "${error == 'errorURL'}"> <p style="color:red"><fmt:message key="errorurl"/> <i><c:out value="${urlPath}" /></i>!</p>			
						<c:set var="error" value="${urlPath}" scope="session"/>

						</c:if>							
			</div>
		</form>				
</div>	
</body>
</html>