<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8" isELIgnored="false" errorPage="ErrorPage.jsp"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>

<!DOCTYPE html>
<fmt:setLocale value="${lang}" scope="session" />
<fmt:setBundle basename="language.messages" scope="session" />
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" type="text/css"
	href="//fonts.googleapis.com/css?family=Open+Sans" />
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>

<style>
img {
	max-width: 100%;
	max-height: 100%;
}

.collapsible {
	background-color: #00819c;
	color: #ffffff;
	cursor: pointer;
	padding: 6px;
	width: 100%;
	border: 1px solid #006b86;
	text-align: left;
	outline: none;
	font-size: 16px;
	border-radius: 4px;
}

.active, .collapsible:hover {
	background-color: #006b86;
}

.collapsible:after {
	content: '\002B';
	color: #f1f1f1;
	font-weight: bold;
	float: right;
	margin-left: 5px;
}

.active:after {
	content: "\2212";
}

.taskbutton {
	background-color: #5bc0de;
	color: #ffffff;
	cursor: pointer;
	padding: 6px;
	padding-left: 18px;
	width: 100%;
	border: 1px solid #46b8da;
	text-align: left;
	outline: none;
	font-size: 16px;
	border-radius: 4px;
}

.content {
	display: block;
	padding: 5px 18px;
	overflow: hidden;
	transition: max-height 0.1s ease-out;
	background-color: #f1f1f1;
}

.contentclosed {
	display: none;
	padding: 5px 18px;
	overflow: hidden;
	transition: max-height 0.1s ease-out;
	background-color: #f1f1f1;
}

.divresize {
	resize: both;
	overflow: hidden;
	height: 65vh;
	display: inline-block;
}

.divresize2 {
	resize: both;
	overflow: hidden;
	height: 65vh;
	width: 65vw;
	display: inline-block;
}

.divaudio {
	overflow: hidden;
	display: inline-block;
}

.divcijena {
	float: right;
}

.divtimer {
	display: inline-block;
	float: right;
}

.gumb {
	background-color: #5bc0de;
	border: none;
	color: white;
	padding: 2px;
	text-decoration: none;
	border: 1px solid #46b8da;
	margin: 2px 2px;
	cursor: pointer;
	border-radius: 4px;
}

.gumb:hover {
	background-color: #46b8da;
}

.gumbrjesenje {
	background-color: #f0ad4e;
	border: none;
	color: white;
	padding: 2px;
	text-decoration: none;
	border: 1px solid #eea236;
	margin: 2px 2px;
	cursor: pointer;
	border-radius: 4px;
}

.gumbrjesenje:hover {
	background-color: #eea236;
}

html * {
	font-size: 16px !important;
	color: #000;
	font-family: Open Sans !important;
	font-weight: normal;
}
</style>

<title><fmt:message key="DSAT" /></title>

</head>

<body>
	<button type="button" class="taskbutton">
		<fmt:message key="task" />
	</button>
	<div class="content">
		<form action="obrada">
			<label for="parametri"><fmt:message key="selectparameter" /></label>
			<input list="parametri" name="podatak" autocomplete="off" required>
			<datalist id="parametri">
				<c:forEach var="element" items="${listParameter}">
					<option value="${element}"></option>
				</c:forEach>
			</datalist>
			<input class="gumb" type="submit"
				value="<fmt:message key="reqparameter"/>">
			<c:if test = "${errorP == 'errorParameter'}"> <p style="color:red"><fmt:message key="errormissingparameter"/> <i><c:out value="${ukloniPodatak}" /></i>!</p>
				<c:set var="errorP" value="${ukloniPodatak}" scope="session"/>		
			</c:if>
		</form>
		<form action="feedback" method="get">
			<input type="hidden" id="myurl" name="myurl" /> <label
				for="dijagnoze"><fmt:message key="selectdiagnosis" /></label> <input
				list="dijagnoze" name="odabranaDijagnoza" autocomplete="off" required>
			<datalist id="dijagnoze">
				<c:forEach var="element" items="${wantedDiagnosesList}">
					<option value="${element}">${element}</option>
				</c:forEach>
			</datalist>
			<input class="gumbrjesenje" type="submit"
				value="<fmt:message key="submitsolution"/>"
				onclick="clearInterval(timerVar)">
			<div class="divtimer" id="timer"></div>
			<label style="padding-right: 4px; float: right;" for="timer"><fmt:message
					key="timer" /></label>
			<c:if test = "${errorP == 'errorDiagnosis'}"> <p style="color:red"><fmt:message key="errormissingdiagnosis"/> <i><c:out value="${odabranaDijagnoza}" /></i>!</p>
				<c:set var="errorP" value="${odabranaDijagnoza}" scope="session"/>		
			</c:if>
			<c:if test = "${errorP == 'errorNoParameters'}"> <p style="color:red"><fmt:message key="errorzeroparameters"/>.</p>
				<c:set var="errorP" value="${odabranaDijagnoza}" scope="session"/>		
			</c:if>
		</form>
	</div>

	<c:set var="listSize" value="${fn:length(listPrint)}" />
	<c:forEach var="i" begin="1" end="${listSize}" step="1"
		varStatus="status">
		<c:set var="element" value="${listPrint[listSize-i]}" />
		<c:choose>
			<c:when test="${not fn:startsWith(element.realValue, 'http')}">
				<button type="button" class="collapsible">${element.name}</button>
				<div class="content">
					<div style="float: left">
						<c:out value="${element.realValue}" />
					</div>
					<div class="divcijena">
						<fmt:message key="cijena" />
						<c:out value="${element.price}" />
					</div>
				</div>
			</c:when>
			<c:otherwise>
				<c:choose>
					<c:when
						test="${fn:contains(element.realValue, '.jpg')||fn:contains(element.realValue, '.jpeg')||fn:contains(element.realValue, '.png')||fn:contains(element.realValue, '.gif')}">
						<c:if test="${status.first}">
							<button type="button" class="collapsible">
								<c:out value="${element.name}" />
							</button>
							<div class="content">
								<div class="divresize">
									<img src="${element.realValue}"
										alt="${element.assignedValue} [Nedostaje slika]" />
								</div>
								<br>
								<div class="divcijena">
									<fmt:message key="cijena" />
									<c:out value="${element.price}" />
								</div>
							</div>
						</c:if>
						<c:if test="${!status.first}">
							<button type="button" class="collapsible">
								<c:out value="${element.name}" />
							</button>
							<div class="contentclosed">
								<div class="divresize">
									<img src="${element.realValue}"
										alt="${element.assignedValue} [Nedostaje slika]" />
								</div>
								<br>
								<div class="divcijena">
									<fmt:message key="cijena" />
									<c:out value="${element.price}" />
								</div>
							</div>
						</c:if>
					</c:when>
					<c:otherwise>
						<c:choose>
							<c:when test="${fn:contains(element.realValue, 'youtube')}">
								<c:if test="${!status.first}">
									<button type="button" class="collapsible">
										<c:out value="${element.name}" />
									</button>
									<div class="contentclosed">
										<div class="divresize2">
											<iframe height="100%" width="100%" src="${element.realValue}"
												frameborder="0"
												allow="accelerometer;  resize; encrypted-media; gyroscope; picture-in-picture"
												allowfullscreen></iframe>
										</div>
										<br>
										<div class="divcijena">
											<fmt:message key="cijena" />
											<c:out value="${element.price}" />
										</div>
									</div>
								</c:if>
								<c:if test="${status.first}">
									<button type="button" class="collapsible">
										<c:out value="${element.name}" />
									</button>
									<div class="content">
										<div class="divresize2">
											<iframe height="100%" width="100%" src="${element.realValue}"
												frameborder="0"
												allow="accelerometer;encrypted-media; autoplay ;gyroscope; picture-in-picture"
												allowfullscreen></iframe>
										</div>
										<br>
										<div class="divcijena">
											<fmt:message key="cijena" />
											<c:out value="${element.price}" />
										</div>
									</div>
								</c:if>
							</c:when>
							<c:otherwise>
								<c:choose>
									<c:when test="${fn:contains(element.realValue, '.mp4')}">
										<c:if test="${!status.first}">
											<button type="button" class="collapsible">
												<c:out value="${element.name}" />
											</button>
											<div class="contentclosed">
												<div class="divresize2">
													<video height="100%" width="100%" controls>
														<source src="${element.realValue}" type="video/mp4">
													</video>
													<br>
												</div>
												<div class="divcijena">
													<fmt:message key="cijena" />
													<c:out value="${element.price}" />
												</div>
											</div>
										</c:if>
										<c:if test="${status.first}">
											<button type="button" class="collapsible">
												<c:out value="${element.name}" />
											</button>
											<div class="content">
												<div class="divresize2">
													<video height="100%" width="100%" controls autoplay>
														<source src="${element.realValue}" type="video/mp4">
													</video>
												</div>
												<br>
												<div class="divcijena">
													<fmt:message key="cijena" />
													<c:out value="${element.price}" />
												</div>
											</div>
										</c:if>
									</c:when>
									<c:otherwise>
										<c:choose>
											<c:when test="${fn:contains(element.realValue, '.pdf')}">
												<c:if test="${!status.first}">
													<button type="button" class="collapsible">
														<c:out value="${element.name}" />
													</button>
													<div class="contentclosed">
														<div class="divresize2">
															<iframe src="${element.realValue}" frameborder="0"
																width="100%" height="100%" /></iframe>
														</div>
														<br>
														<div class="divcijena">
															<fmt:message key="cijena" />
															<c:out value="${element.price}" />
														</div>
													</div>
												</c:if>
												<c:if test="${status.first}">
													<button type="button" class="collapsible">
														<c:out value="${element.name}" />
													</button>
													<div class="content">
														<div class="divresize2">
															<iframe src="${element.realValue}" frameborder="0"
																width="100%" height="100%" /></iframe>
														</div>
														<br>
														<div class="divcijena">
															<fmt:message key="cijena" />
															<c:out value="${element.price}" />
														</div>
													</div>
												</c:if>
											</c:when>
											<c:otherwise>
												<c:choose>
													<c:when test="${fn:contains(element.realValue, '.mp3')}">
														<c:if test="${!status.first}">
															<button type="button" class="collapsible">
																<c:out value="${element.name}" />
															</button>
															<div class="contentclosed">
																<div class="divaudio">
																	<audio controls>
																		<source src="${element.realValue}" type="audio/mpeg">
																	</audio>
																</div>
																<br>
																<div class="divcijena">
																	<fmt:message key="cijena" />
																	<c:out value="${element.price}" />
																</div>
															</div>
														</c:if>
														<c:if test="${status.first}">
															<button type="button" class="collapsible">
																<c:out value="${element.name}" />
															</button>
															<div class="content">
																<div class="divaudio">
																	<audio controls>
																		<source src="${element.realValue}" type="audio/mpeg">
																	</audio>
																</div>
																<br>
																<div class="divcijena">
																	<fmt:message key="cijena" />
																	<c:out value="${element.price}" />
																</div>
															</div>
														</c:if>
													</c:when>
													<c:otherwise>
														<c:choose>
															<c:when test="${fn:contains(element.realValue, '.ogg')}">
																<c:if test="${!status.first}">
																	<button type="button" class="collapsible">
																		<c:out value="${element.name}" />
																	</button>
																	<div class="contentclosed">
																		<div class="divaudio">
																			<audio controls>
																				<source src="${element.realValue}" type="audio/ogg">
																			</audio>
																		</div>
																		<br>
																		<div class="divcijena">
																			<fmt:message key="cijena" />
																			<c:out value="${element.price}" />
																		</div>
																	</div>
																</c:if>
																<c:if test="${status.first}">
																	<button type="button" class="collapsible">
																		<c:out value="${element.name}" />
																	</button>
																	<div class="content">
																		<div class="divaudio">
																			<audio controls>
																				<source src="${element.realValue}" type="audio/ogg">
																			</audio>
																		</div>
																		<br>
																		<div class="divcijena">
																			<fmt:message key="cijena" />
																			<c:out value="${element.price}" />
																		</div>
																	</div>
																</c:if>
															</c:when>
															<c:otherwise>
																<c:choose>
																	<c:when
																		test="${fn:contains(element.realValue, '.wav')}">
																		<c:if test="${!status.first}">
																			<button type="button" class="collapsible">
																				<c:out value="${element.name}" />
																			</button>
																			<div class="contentclosed">
																				<div class="divaudio">
																					<audio controls>
																						<source src="${element.realValue}"
																							type="audio/wav">
																					</audio>
																				</div>
																				<br>
																				<div class="divcijena">
																					<fmt:message key="cijena" />
																					<c:out value="${element.price}" />
																				</div>
																			</div>
																		</c:if>
																		<c:if test="${status.first}">
																			<button type="button" class="collapsible">
																				<c:out value="${element.name}" />
																			</button>
																			<div class="content">
																				<div class="divaudio">
																					<audio controls>
																						<source src="${element.realValue}"
																							type="audio/wav">
																					</audio>
																				</div>
																				<br>
																				<div class="divcijena">
																					<fmt:message key="cijena" />
																					<c:out value="${element.price}" />
																				</div>
																			</div>
																		</c:if>
																	</c:when>
																</c:choose>
															</c:otherwise>
														</c:choose>
													</c:otherwise>
												</c:choose>
											</c:otherwise>
										</c:choose>
									</c:otherwise>
								</c:choose>
							</c:otherwise>
						</c:choose>
					</c:otherwise>
				</c:choose>
			</c:otherwise>
		</c:choose>
	</c:forEach>

	<script type="text/javascript">
		document.getElementById('myurl').value = window.location.href
	</script>

	<script>
		var coll = document.getElementsByClassName("collapsible");
		var i;

		for (i = 0; i < coll.length; i++) {
			coll[i].addEventListener("click", function() {
				this.classList.toggle("active");
				var content = this.nextElementSibling;
				if (content.style.display === "block") {
					content.style.display = "none";
				} else {
					content.style.display = "block";
				}
			});
		}
	</script>

	<script>
		var totalTime = sessionStorage.getItem("ukupnoVrijeme");
		var timerVar = setInterval(countTimer, 1000);
		function countTimer() {
			++totalTime;
			var hour = Math.floor((totalTime) / 3600);
			var minute = Math.floor(((totalTime) - hour * 3600) / 60);
			var seconds = (totalTime) - (hour * 3600 + minute * 60);
			if (hour < 10)
				hour = "0" + hour;
			if (minute < 10)
				minute = "0" + minute;
			if (seconds < 10)
				seconds = "0" + seconds;
			sessionStorage.setItem("ukupnoVrijeme", totalTime);
			document.getElementById("timer").innerHTML = hour + ":" + minute
					+ ":" + seconds;
		}
	</script>

</body>
</html>