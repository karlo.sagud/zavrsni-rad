<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8" isELIgnored="false" errorPage="ErrorPage.jsp"%>

<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>

<!DOCTYPE html>
<fmt:setLocale value="${lang}" scope="session" />
<fmt:setBundle basename="language.messages" scope="session" />
<html>
<head>

<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
<link rel="stylesheet" type="text/css"
	href="//fonts.googleapis.com/css?family=Open+Sans" />
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
<script src="bootstrap/js/jquery.js"></script>
<script src="bootstrap/js/bootstrap-alert.js"></script>
<script src="bootstrap/js/bootstrap-modal.js"></script>
<script src="bootstrap/js/bootstrap-transition.js"></script>
<script src="bootstrap/js/bootstrap-tooltip.js"></script>

<style>
.collapsible {
	background-color: #00819c;
	color: #ffffff;
	cursor: pointer;
	padding: 6px;
	padding-left: 18px;
	width: 100%;
	border: 1px solid #006b86;
	text-align: left;
	outline: none;
	font-size: 16px;
	border-radius: 4px;
}

.collapsiblemanji {
	background-color: #5bc0de;
	color: #ffffff;
	cursor: pointer;
	padding: 3px;
	padding-left: 18px;
	width: 25%;
	border: 1px solid #46b8da;
	text-align: left;
	outline: none;
	font-size: 16px;
	border-radius: 4px;
	display: block;
}

.gumb {
	background-color: #5bc0de;
	border: none;
	color: white;
	padding: 2px;
	text-decoration: none;
	border: 1px solid #46b8da;
	margin: 2px 2px;
	cursor: pointer;
	border-radius: 4px;
}

.gumb:hover {
	background-color: #46b8da;
}

.content {
	display: block;
	padding: 5px 18px;
	overflow: hidden;
	transition: max-height 0.1s ease-out;
	background-color: #f1f1f1;
}

.contenttimer {
	display: inline-block;
	padding: 5px 18px;
	overflow: hidden;
	background-color: #f1f1f1;
}

.tooltip-inner {
	max-width: 800px;
	/* If max-width does not work, try using width instead */
	width: 800px;
}

.sat {
	display: inline;
	overflow: hidden;
	transition: max-height 0.1s ease-out;
	background-color: #f1f1f1;
}

u {
	text-decoration: underline dotted black;
}

html * {
	font-size: 16px !important;
	color: #000;
	font-family: Open Sans !important;
}
</style>

<title><fmt:message key="title3" /></title>

</head>

<body>
	<button type="button" class="collapsible"><fmt:message key="rezultat" /></button>	
	<div class="content">	
	<c:if test="${poslanOdgovor == tocnaDijagnoza}">
			<c:out value="${poslanOdgovor}" />
			(<i><a href="${workingCaseLink}"><c:out value="${workingCase}" /></a></i>) <fmt:message key="iscorrectdiag" />
			<br> 
	</c:if>
	<c:if test="${poslanOdgovor != tocnaDijagnoza}">
			<c:out value="${poslanOdgovor}" /> <fmt:message key="nijetocno" />
			<c:out value="${tocnaDijagnoza}"/> (<i><a href="${workingCaseLink}"><c:out value="${workingCase}" /></a></i>).<br>
	</c:if>
			<u><a href="#" rel="tooltip" data-placement="bottom"
				title="<fmt:message key="tooltip1"/>"><fmt:message key="vjerojatnost1" /></a></u>
			<c:forEach var="element" items="${vjerojatnostIspravno}"
				varStatus="status">
				<c:if test="${status.last}">
					<fmt:formatNumber value="${element}" pattern="0.00" />%
					<c:if test="${element < 100.00}">
						<br>
						<fmt:message key="otherpossiblesolutions" />
						<div class="content">
							<c:forEach var="element" items="${diagnosisSortWanted}"
								varStatus="status">
							> <c:out value="${element.dijagnoza}" />, <fmt:formatNumber
									value="${possibleSolutionsVjerojatnost[status.index]}"
									pattern="0.00" />%
							(<c:forEach var="case" items="${diagnosisSortWantedCopy}"
									varStatus="stat">
									<c:if test="${element.dijagnoza == case.dijagnoza}">
										<i><a href="${workingCasesLinkList[stat.index]}"><c:out value="${case.caseBroj}" /></a></i>										
									</c:if>
								</c:forEach>)
								<br>
							</c:forEach>
						</div>
					</c:if>
					<c:if test="${element > 99.99}">
						<br>
					</c:if>
				</c:if>
			</c:forEach>
			<fmt:message key="ukupnovrijeme"/>: <div class="sat" class="content" id="timer"></div>
			<br>
			<fmt:message key="ukupnacijena" />
			<c:out value="${ukupnaCijena}" />
		</div>
		
		<button type="button" class="collapsible">
			<fmt:message key="feedback" />
		</button>
		<div class="content">
			<button type="button" class="collapsiblemanji">
				<fmt:message key="diagprocedure" />
			</button>
			<div class="content">
				<u><a href="#" rel="tooltip" data-placement="bottom"
					title="<fmt:message key="tooltip2"/>"><fmt:message key="slijed" />
				</a></u>
				<div class="content">
					<fmt:message key="format" />
					<br>
					<c:forEach var="element" items="${redoslijedPozvanihParametara}" varStatus="status">
						<c:out value="${status.index+1}" />. <c:out value="${element}" /> - 
				<c:out
							value="${redoslijedVrijednostiPozvanihParametara[status.index]}" />, 
				[<c:out value="${timeOnParameter[status.index]}" />], 
				<fmt:formatNumber value="${vjerojatnostIspravno[status.index]}"
							pattern="0.00" />%
				<br>
					</c:forEach>
				</div>
			</div>
			
			<c:if test="${fn:length(redundantniParametri) > 0}">
				<button type="button" class="collapsiblemanji">
					<fmt:message key="redun" />
				</button>
				<div class="content">
					<u><a href="#" rel="tooltip" data-placement="bottom"
						title="<fmt:message key="tooltip3"/>"> <fmt:message key="noinfo" />
					</a></u>
					<div class="content">
						<c:forEach var="element" items="${redundantniParametri}"
							varStatus="status">
						> <c:out value="${element}" />
							<br>
						</c:forEach>
					</div>
					<fmt:message key="skraceni" />
					<div class="content">
						<c:forEach var="element" items="${bitniParametri}" varStatus="status">
							<c:out value="${status.index+1}" />. <c:out value="${element}" /><br>
						</c:forEach>
					</div>
					<fmt:message key="cijenaskraceni" />
					<c:out value="${ukupnaMinCijena}" />
					<br>
				</div>
			</c:if>
			
			<c:if test="${fn:length(nepozvaniParametri) > 0}">
				<button type="button" class="collapsiblemanji">
					<fmt:message key="missingpara" />
				</button>
				<div class="content">
						<c:forEach var="element" items="${nepozvaniParametri}" varStatus="status">
						> <c:out value="${element.name}" /> - <c:out value="${element.assignedValue}" />
							<br>
						</c:forEach>					
				</div>
			</c:if>
		</div>

	<button type="button" class="collapsible">
		<fmt:message key="ponovno" />
	</button>
	<div class="content">
		<a href="${myurl}"><fmt:message key="poveznica" /></a>
	</div>

	<script>
		var totalTime = sessionStorage.getItem("ukupnoVrijeme");
		var hour = Math.floor((totalTime) / 3600);
		var minute = Math.floor(((totalTime) - hour * 3600) / 60);
		var seconds = (totalTime) - (hour * 3600 + minute * 60);
		if (hour < 10)
			hour = "0" + hour;
		if (minute < 10)
			minute = "0" + minute;
		if (seconds < 10)
			seconds = "0" + seconds;
		document.getElementById("timer").innerHTML = hour + ":" + minute + ":"
				+ seconds;
	</script>

	<script type="text/javascript">
		$(function() {
			$("[rel='tooltip']").tooltip();
		});
	</script>

	<script>
		sessionStorage.setItem("ukupnoVrijeme", 0);
	</script>
</body>
</html>